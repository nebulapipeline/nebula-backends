# -*- coding: utf8 -*-
from __future__ import print_function
import abc
import os
import re
import subprocess
import csv
import logging

import nebula
from nebula.common import core, util
from nebula.common.util import symlinks
from nebula.auth import user


root_path = util.dirname(__file__, 2)
rsc_path = os.path.join(root_path, 'resources')

server = user.get_server()  # for querying the tactic

base_dir_key = '%s_client_repo_dir' % ('win32' if os.name == 'nt' else 'linux')
client_repo_dir = server.get_base_dirs()[base_dir_key]

tactic_backend = core.Backend('tactic')  # for use in GUI


class SObjectField(object):
    '''
    Descriptor to return sobject filed value from given field name
    Helpful especially to set and update a field in the db
    '''

    def __init__(self, key, default=None):
        self._key = key
        self._default = default

    def __get__(self, instance, owner):
        if instance is not None:
            try:
                return instance[self._key]
            except KeyError:
                if self._default is None:
                    raise
                else:
                    return self._default
        return self

    def __set__(self, instance, value):
        instance[self._key] = value
        instance._dirty = True


class SObjectMeta(abc.ABCMeta):
    def __new__(cls, name, bases, namespace):
        cls = super(SObjectMeta, cls).__new__(cls, name, bases, namespace)
        fields = dict()
        for base in bases:
            fields.update(getattr(base, "_fields", {}))
        fields = {
            name: value
            for name, value in namespace.items()
            if isinstance(value, SObjectField)
        }
        cls._fields = fields
        return cls


class SObject(core.IEntity, dict):
    __metaclass__ = SObjectMeta
    '''
    base class for common operations on all entity objects
    '''

    def __new__(cls, *args, **kwargs):
        if hasattr(cls, '__abstractmethods__') and len(
                cls.__abstractmethods__) > 0:
            raise TypeError(
                    "Can't instantiate abstract class {classname} with"
                    " abstract methods {abstractmethods}".format(
                        classname=cls.__name__,
                        abstractmethods=', '.join(cls.__abstractmethods__)))
        return super(SObject, cls).__new__(cls, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.__dict__ = self
        super(SObject, self).__init__(*args, **kwargs)
        self._thumb = None  # cache the thumb path
        self._path = ''  # cache the path
        self._snapshots = []
        self._dirty = False

    @abc.abstractproperty
    def __stype__(self):
        pass

    @classmethod
    def setProject(cls, project):
        global _current_project
        server.set_project(project)
        if project:
            _current_project = Project.get(filters=[('code', project)])
        else:
            _current_project = Project.get(filters=[('code', 'admin')])

    @classmethod
    def getProject(cls):
        global _current_project
        return _current_project

    @classmethod
    def _cleanup_filters(cls, filters=None):
        filters = cls._convert_fields(filters)
        if not filters:
            return
        _result = []
        for condition in filters:
            if len(condition) >= 3:
                condition = tuple(condition[:-1] + (str(condition[-1]),))
            _result.append(condition)
        return _result

    @classmethod
    def _convert_fields(cls, data, restrict=False):
        '''Convert fields from data into correspoding tactic database fields.
        Data can be a list of fields, filters, dicts or simply a solitary dict.
        Incase a None is passed a None is returned

        :type data: list or dict or None
        :rtype: list or dict or None
        '''
        if data is None:
            return None
        result = dict() if isinstance(data, dict) else []

        for item in data:

            default = None
            newitem = key = item
            if item and isinstance(item, (list, tuple)):
                key = item[0]

            if isinstance(item, dict):
                newitem = cls._convert_fields(item)

            elif key in cls._fields:
                newkey = cls._fields[key]._key
                default = cls._fields[key]._default
                if isinstance(item, (list, tuple)):
                    newitem = (newkey,) + tuple(item[1:])
                else:
                    newitem = newkey

            elif re.match('_[A-Za-z0-9]+(__[A-Za-z0-9_])*', key) is not None:
                continue

            elif re.match('__search_key__', key) is not None:
                continue

            if isinstance(result, dict):
                value = data[item]
                if value is not None:
                    result[newitem] = data[item]
            else:
                result.append(newitem)

        return result

    @classmethod
    def _get_sobjects(cls, filters=None, order_bys=None):
        return [cls(data) for data in server.query(
                cls.__stype__,
                filters=cls._cleanup_filters(filters),
                order_bys=cls._convert_fields(order_bys))]

    @classmethod
    def all(cls, order_bys=None):
        return cls._get_sobjects(filters=None, order_bys=order_bys)

    @classmethod
    def get(cls, filters=None, order_bys=None):
        sobject = cls._get_sobjects(filters, order_bys)
        return cls(sobject[0]) if sobject else None

    @classmethod
    def filter(cls, filters=None, order_bys=None):
        return cls._get_sobjects(filters=filters, order_bys=order_bys)

    @classmethod
    def create(cls, sobj):
        sobj = cls._convert_fields(sobj)
        return cls(server.insert(cls.__stype__, sobj))

    @classmethod
    def createMultiple(cls, sobjs):
        return [cls(obj) for obj in server.insert_multiple(
                                cls.__stype__,
                                cls._convert_fields(sobjs))]

    @classmethod
    def saveMultiple(cls, sobjs):
        result = []
        unchanged = []
        new = []
        dirty = []

        for idx, sobj in enumerate(sobjs):
            if isinstance(sobj, cls):
                if SObject.exists(sobj):
                    if SObject.isDirty(sobj):
                        dirty.append((sobj, idx))
                    else:
                        unchanged.append((sobj, idx))
                else:
                    new.append((sobj, idx))
            else:
                unchanged.append((sobj, idx))

        if new:
            _new = zip(
                cls.createMultiple(zip(*new)[0]),
                zip(*new)[1])
            result.extend(_new)

        if dirty:
            update_data = {
                sobj.__search_key__: cls._convert_fields(sobj)
                        for sobj in zip(*dirty)[0]
            }
            _dirty = zip(
                [cls(obj) for obj in server.update_multiple(update_data)],
                zip(*dirty)[1])
            for elem, _ in dirty:
                elem._dirty = False
            result.extend(_dirty)

        result.extend(unchanged)

        # preserving original order
        result = sorted(result, key=lambda x: x[1])
        if result:
            result = zip(*result)[0]

        return result

    @classmethod
    def removeMultiple(cls, items, include_dependencies=False):
        server.delete_sobject([item.__search_key__ for item in items],
                              include_dependencies=include_dependencies)

    def update(self, iterable, **words):
        self._dirty = True
        dict.update(self, iterable, **words)

    def save(self):
        # remove the elements with none values
        sobj = {k: v for k, v in self.items() if v is not None}
        # remote the arbitrary varialble created on
        # instances with pattern self.__name
        for key in sobj.keys():
            if re.match('_[A-Za-z0-9]+(__[A-Za-z0-9_])*', key) is not None:
                del sobj[key]
        if hasattr(self, '__search_key__'):
            server.update(self.__search_key__, sobj)
            self._dirty = False
        else:
            obj = server.insert(self.__stype__, sobj)
            self.update(obj)
            self._dirty = False

    @property
    def __search_key__(self):
        return self['__search_key__']
    identity = __search_key__

    def exists(self):
        return hasattr(self, '__search_key__') and self.__search_key__

    def isDirty(self):
        return self._dirty

    @property
    def key(self):
        try:
            return self.code
        except (KeyError, AttributeError):
            return self.id

    @property
    def path(self):
        '''
        file system path of the sobject
        '''
        if self._path:
            return self._path
        path = server.get_virtual_snapshot_path(self.__search_key__)
        path = util.dirname(path, 2)
        path = symlinks.translatePath(
            path,
            maps=symlinks.getSymlinks(
                server.get_base_dirs()[base_dir_key]))
        self._path = path
        return self._path

    def remove(self, include_dependencies=False):
        '''
        remove this sobject from database
        '''
        server.delete_sobject(
                self.__search_key__,
                include_dependencies=include_dependencies)

    def browseLocation(self):
        if self.path:
            path = os.path.normpath(self.path)
            if os.path.exists(path):
                subprocess.call('explorer %s' % path, shell=True)

    def get_thumbnail(self, copy_server=False):
        if self._thumb:
            return self._thumb
        icon_path = ''
        _s = server
        if copy_server:
            _s = server.copy()
        snapshot = _s.get_snapshot(self.__search_key__,
                                   context='icon',
                                   version=-1)
        logging.debug('thumb %r' % _s)
        if snapshot:
            icon_path = _s.get_all_paths_from_snapshot(
                    snapshot, mode='client_repo')
            icon_path = icon_path[1]
            if nebula.DEBUG:
                icon_path = icon_path.replace('/home/apache/assets',
                                              'c:/Users/qurban.ali/share')
            self._thumb = icon_path
        return icon_path

    def set_thumbnail(self, path):
        if not path:
            return
        server.simple_checkin(
                self.__search_key__, context='icon', file_path=path,
                mode='copy')
        self._thumb = path

    thumbnail = property(get_thumbnail, set_thumbnail)

    def get_snapshots(self, copy_server=False):
        '''
        returns the snapshots within a sobject
        '''
        if self._snapshots:
            return self._snapshots
        _s = server
        if copy_server:
            _s = server.copy()
        logging.debug('snap %r' % _s)
        self._snapshots[:] = [
                Snapshot(snap) for snap in server.query_snapshots(filters=[
                    ('search_code', self.code),
                    ('project_code', self.getProject().code)],
                                    include_paths_dict=True,
                                    order_bys=['context', 'version'])
                if snap['context'] != 'icon']
        return self._snapshots

    snapshots = property(get_snapshots)

    def createSnapshot(self, snapshot):
        '''
        creates a snapshot with single file
        '''
        server.simple_checkin(self.__search_key__,
                              context=snapshot.context,
                              mode='copy',
                              file_path=snapshot.files[0])

    def fields_data(self):
        data = {}
        for field in self._fields.values():
            try:
                data[field._key] = getattr(self, field._key)
            except (AttributeError, KeyError):
                pass
        return data

    def createEmptySnapshot(self, search_key, context):
        return Snapshot(server.create_snapshot(search_key, context))


class Project(core.IProject, SObject):

    TEMPLATE_EPISODIC = 'Episodic'
    TEMPLATE_COMMERCIAL = 'Commercial'

    CATEGORY_INHOUSE = 'Internal'
    CATEGORY_EXTERNAL = 'External'
    CATEGORY_LOCAL = 'Local'

    __templates__ = [TEMPLATE_EPISODIC, TEMPLATE_COMMERCIAL]
    __categories__ = [CATEGORY_INHOUSE, CATEGORY_EXTERNAL, CATEGORY_LOCAL]
    __stype__ = 'sthpw/project'
    COMMAND = 'tactic.command.ProjectTemplateInstallerCmd'

    # r'c:\Program '
    # 'Files\Southpaw\tactic\src\install\start\templates\vfx-1.0.0.zip'

    TEMPLATE_VFX = ''
    if nebula.DEBUG:
        if os.environ.get('USERNAME', os.environ.get('USER', '')) == '123':
            TEMPLATE_VFX = \
                r"C:\ProgramData\Tactic\data\templates\vfx-1.0.0.zip"
        else:
            TEMPLATE_VFX = "/home/apache/tactic_data/templates/vfx-1.0.0.zip"
    THEME_DEFAULT = "TACTIC/default_theme"
    TEMPLATE_CODE_VFX = "vfx"

    # define attrs which need to be set not only get
    title = SObjectField('title')
    category = SObjectField('category')

    def __init__(self, *args, **kwargs):
        super(Project, self).__init__(*args, **kwargs)
        self._template = None  # cache the template name

    @classmethod
    def all(cls):
        # exclude the tactic admin projects
        projects = super(Project, cls).all()
        return [pro for pro in projects if pro.type == 'vfx']

    @property
    def active(self):
        return self.status == 'in_progres'

    def get_template(self, copy_server=False):
        # TODO: make use of Episode/productionElement class
        if self._template:
            return self._template
        _s = server
        if copy_server:
            _s = server.copy()
        try:
            p = _s.get_project()
            _s.set_project(self.code)
            if _s.query('vfx/episode', filters=[('code', 'default')]):
                self._template = Project.TEMPLATE_COMMERCIAL
            else:
                self._template = Project.TEMPLATE_EPISODIC
            _s.set_project(p)
            return self._template
        except:
            return ''

    template = property(get_template)

    def __get_path(self):
        if self._path:
            return self._path
        _path = os.path.join(client_repo_dir, self.code)
        path = symlinks.translateSymlink(_path)
        if not util.paths_equal(_path, path):
            self._path = path
        return self._path

    def __set_path(self, path):
        self.createShortcuts(path)

    path = property(__get_path, __set_path)

    def remove(self):
        '''
        remove the project from database including it's shortcuts
        '''
        # TODO:
        pass

    @classmethod
    def create(cls,
               title,
               path,
               category,
               logo='',
               template=TEMPLATE_EPISODIC,
               create_shortcuts=True):
        '''
        creates a project on tactic
        '''
        data = {}
        data['force_database'] = True
        data['template_code'] = Project.TEMPLATE_CODE_VFX
        data['path'] = Project.TEMPLATE_VFX
        data['project_theme'] = Project.THEME_DEFAULT
        data['project_code'] = '_'.join(title.split()).lower()
        data['project_title'] = title
        server.execute_cmd(cls.COMMAND, data)
        project = cls.get(filters=[('code', data['project_code'])])
        if template == cls.TEMPLATE_COMMERCIAL:
            # create the default episode
            # TODO: make use of Episode/productionElement class
            p = server.get_project()
            server.set_project(project.code)
            server.insert('vfx/episode', {'code': 'default'})
            server.set_project(p)
        # specify the category of the project
        if category:
            project.category = category
        # add thumbnail
        if logo:
            project.thumbnail = logo
        project.createNaming(
                os.path.join(rsc_path, 'tactic_project_naming.csv'))
        # create the shortcuts
        if create_shortcuts:
            project.createShortcuts(path)
        return project

    def createShortcuts(self, path):
        '''
        Creates shortcuts in base asset directory for project folders living
        in different network drives
        '''
        global client_repo_dir
        client_dir = os.path.dirname(client_repo_dir)
        path = os.path.expanduser(path)
        path = os.path.normpath(path)

        if os.name == 'nt':
            cmd = r'''mkdir {path}\map
            mkdir {path}\unc
            rmdir {path}\map\{code}
            rmdir {path}\unc\{code}
            mklink /d {path}\map\{code} {project_path}
            mklink /d {path}\unc\{code} {project_path_unc}'''.format(
                path=os.path.normpath(client_dir),
                code=self.code,
                project_path=path,
                project_path_unc=util.translateMappedtoUNC(path))
            batfile = os.path.join(client_dir, 'mklink_%s.bat' % self.code)
            with open(batfile, 'w') as f:
                f.write(cmd)
            # user tactic should not be in administrator group on
            # win7, this is not applicable to win10.
            # Tactic user should be allowed to create symlinks
            info = util.CreateProcessWithLogonW(lpUsername='tactic',
                                                lpDomain='iceanimations.com',
                                                lpPassword='tactic123',
                                                lpCommandLine=batfile)
            util.waitForChild(info)
        else:
            os.symlink(path, os.path.join(client_dir, 'lnk', self.code))
            os.symlink(path, os.path.join(client_dir, 'clnk', self.code))

    def createNaming(self, path):
        '''
        creates naming convensions on tactic
        args:
            path (str): path to csv file
        '''
        project = self.getProject()
        self.setProject(self.code)
        with open(path, 'rb') as csvfile:
            data = csv.reader(csvfile)
            data = list(data)
            if data:
                headers = data.pop(0)
                sobjects = []
                for sobject in data:
                    sobjects.append(
                            {headers[i]: val for i, val in enumerate(sobject)})
                if sobjects:
                    for naming in _Naming.all():
                        server.delete_sobject(naming.__search_key__)
                    _Naming.createMultiple(sobjects)
        self.setProject(project.code)

    def getProductionElementTypes(self):
        types = ['Episode', 'Sequence', 'Shot']
        if self.template == Project.TEMPLATE_COMMERCIAL:
            _ = types.pop(0)
        return types

    def getProductionElements(self):
        if self.template == Project.TEMPLATE_EPISODIC:
            return Episode.all()
        elif self.template == Project.TEMPLATE_COMMERCIAL:
            return Sequence.all()
        else:
            pass


def _set_current_project():
    global _current_project
    project = server.get_project()
    if project:
        proj = Project.get(filters=[('code', project)])
        if proj:
            _current_project = proj
            return
    _current_project = Project.get(filters=[('code', 'admin')])


#  _set_current_project()


class Snapshot(core.ISnapshot, SObject):
    __stype__ = 'sthpw/snapshot'

    def __init__(self, *args, **kwargs):
        super(Snapshot, self).__init__(*args, **kwargs)

    @property
    def files(self):
        try:
            return [
                path for path_list in self['__paths_dict__'].values()
                for path in path_list
            ]
        except KeyError:
            return []

    @property
    def parent_code(self):
        return self.search_code

    def addFiles(self, paths):
        for path in paths:
            server.add_file(self.code, path, mode='copy')
        # update the snapshot to have the new files
        snap = server.query_snapshots(
                filters=[('code', self.code)], include_paths_dict=True)
        self.update(snap[0])

    def removeFiles(self, names):
        # TODO : find out if this method is even used
        from xml.etree import cElementTree as ET
        tree = ET.fromstring(self.snapshot)
        files = tree.findall('file')
        file_codes = []
        for phile in files:
            if phile.get('name') in names:
                file_codes.append(phile.get('file_code'))
        if file_codes:
            # reuse the variable, get all the files
            files[:] = File.filter(filters=[('code', file_codes)])
            if files:
                for phile in files:
                    phile.remove()


class File(core.IFile, SObject):
    __stype__ = 'sthpw/file'


class Category(core.ICategory, SObject):  # TODO: rename to AssetCategory
    __stype__ = 'vfx/asset_category'

    name = SObjectField('code')

    def __init__(self, *args, **kwargs):
        super(Category, self).__init__(*args, **kwargs)


class Asset(core.IAsset, SObject):
    __stype__ = 'vfx/asset'

    name = SObjectField('code')
    category = SObjectField('asset_category')
    pipeline_code = SObjectField('pipeline_code')

    def __init__(self, *args, **kwargs):
        super(Asset, self).__init__(*args, **kwargs)
        self._productionElement = None  # cache the productionElement

    def get_productionElement(self, copy_server=False):
        # TODO: make use of Episode/productionElement class
        if self._productionElement:
            return self._productionElement
        _s = server
        if copy_server:
            _s = _s.copy()
        logging.debug('prod %r' % _s)
        try:
            if self.getProject().template == Project.TEMPLATE_EPISODIC:
                episodes = _s.query('vfx/asset_in_episode',
                                    filters=[('asset_code', self.code)],
                                    columns=['episode_code'])
                self._productionElement = [
                    ep['episode_code'] for ep in episodes
                ]
            elif self.getProject().template == Project.TEMPLATE_COMMERCIAL:
                sequences = _s.query('vfx/asset_in_sequence',
                                     filters=[('asset_code', self.code)],
                                     columns=['sequence_code'])
                self._productionElement = [
                    seq['sequence_code'] for seq in sequences
                ]
            else:
                pass
            return self._productionElement
        except:
            return ''

    productionElement = property(get_productionElement)


class ProductionElement(core.IProductionElement, SObject):
    '''Parent Class for Episode, Sequence, Shot'''
    name = SObjectField('code')
    sort_order = SObjectField('sort_order', 0)
    description = SObjectField('description', '')
    s_status = SObjectField('s_status', '')

    def __init__(self, *args, **kwargs):
        super(ProductionElement, self).__init__(*args, **kwargs)

    @util.cached_property
    def preceding(self):
        objs = self.filter(filters=[
            ('sort_order', '<', self.sort_order)],
            order_bys=['sort_order desc'])
        if objs:
            objs = [obj for obj in objs
                    if obj.sort_order == objs[0].sort_order]
        return objs
    get_preceding = preceding.uncached_getter()

    @util.cached_property
    def succeeding(self):
        objs = self.filter(filters=[('sort_order', '>', self.sort_order)],
                           order_bys=['sort_order'])
        if objs:
            objs = [obj for obj in objs
                    if obj.sort_order == objs[0].sort_order]
        return objs
    get_succeeding = succeeding.uncached_getter()

    @util.cached_property
    def overlapping(self):
        return self.filter([
            ('sort_order', self.sort_order),
            ('code', '!=', self.code)])
    get_overlapping = succeeding.uncached_getter()

    @abc.abstractmethod
    def get_children(self, child_type, copy_server=False):
        _s = server
        if copy_server:
            _s = _s.copy()
        return _s.get_all_children(self.__search_key__, child_type)

    @abc.abstractmethod
    def get_parent(self, copy_server=False):
        _s = server
        if copy_server:
            _s = _s.copy()
        return _s.get_parent(self.__search_key__)


class Episode(ProductionElement, core.IEpisode):
    __stype__ = 'vfx/episode'

    def __init__(self, *args, **kwargs):
        super(Episode, self).__init__(*args, **kwargs)

    @util.cached_property
    def children(self, copy_server=False):
        return [Sequence(seq) for seq in super(Episode, self).get_children(
            Sequence.__stype__, copy_server=copy_server)]
    get_children = children.uncached_getter()

    def get_parent(self, copy_server=False):
        return None
    parent = property(get_parent)

    @util.cached_property
    def sequences(self, copy_server=False):
        return self.get_children(copy_server=copy_server)
    get_sequences = sequences.uncached_getter()

    @util.cached_property
    def shots(self, copy_server=False):
        _s = server
        if copy_server:
            _s = server.copy()
        return [
                Shot(sh) for sh in _s.eval(
                    "@SOBJECT(vfx/sequence.vfx/shot)",
                    search_keys=[self.__search_key__])]
    get_shots = shots.uncached_getter()

    @classmethod
    def create(self, sobj):
        project = self.getProject()
        if project.template == Project.TEMPLATE_EPISODIC:
            return super(Episode, self).create(sobj)
        else:
            raise Exception(
                'Must not Create Episode "%s" in non-episodic project "%s"' % (
                    sobj['code'], project.code))


class Sequence(ProductionElement, core.ISequence):
    __stype__ = 'vfx/sequence'

    episode = SObjectField('episode_code')

    def __init__(self, *args, **kwargs):
        super(Sequence, self).__init__(*args, **kwargs)

    @util.cached_property
    def children(self, copy_server=False):
        return [Sequence(seq) for seq in super(Sequence, self).get_children(
            Shot.__stype__, copy_server=copy_server)]
    get_children = children.uncached_getter()

    @util.cached_property
    def parent(self, copy_server=False):
        obj = super(Sequence, self).get_parent(copy_server=copy_server)
        return Episode(obj) if obj else None
    get_parent = parent.uncached_getter()

    @util.cached_property
    def shots(self, copy_server=False):
        _s = server
        if copy_server:
            _s = server.copy()
        return [Shot(sh) for sh in _s.get_all_children(
            self.__search_key__, Shot.__stype__)]
    get_shots = shots.uncached_getter()

    @util.cached_property
    def preceding(self):
        objs = self.filter(
                filters=[
                    ('sort_order', '<', self.sort_order),
                    ('episode_code', self.episode_code)],
                order_bys=['sort_order desc'])
        if objs:
            objs = [obj
                    for obj in objs
                    if obj.sort_order == objs[0].sort_order]
        return objs
    get_preceding = preceding.uncached_getter()

    @util.cached_property
    def succeeding(self):
        objs = self.filter(
                filters=[
                    ('sort_order', '>', self.sort_order),
                    ('episode_code', self.episode_code)],
                order_bys=['sort_order'])
        if objs:
            objs = [obj for obj in objs
                    if obj.sort_order == objs[0].sort_order]
        return objs
    get_succeeding = succeeding.uncached_getter()

    @util.cached_property
    def overlapping(self):
        return [Sequence(seq) for seq in self.filter(
            filters=[
                ('sort_order', self.sort_order),
                ('episode_code', self.episode),
                ('code', '!=', self.code)])]
    get_overlapping = overlapping.uncached_getter()


class Shot(ProductionElement, core.IShot):
    __stype__ = 'vfx/shot'

    start_frame = SObjectField('tc_frame_start', 0)
    end_frame = SObjectField('tc_frame_end', 0)
    frame_in = SObjectField('frame_in', 0)
    frame_out = SObjectField('frame_out', 0)

    status = SObjectField('status', '')
    parent_code = SObjectField('parent_code', '')
    images = SObjectField('images', '')
    frame_note = SObjectField('frame_note', '')
    short_code = SObjectField('short_code', '')
    priority = SObjectField('priority', '')
    complexity = SObjectField('complexity', '')
    type = SObjectField('type', '')
    scan_status = SObjectField('scan_status', '')

    sequence = SObjectField('sequence_code', '')

    def get_children(self, copy_server=False):
        return []
    children = property(get_children)

    @util.cached_property
    def parent(self, copy_server=False):
        return Sequence(super(Shot, self).get_parent(copy_server=copy_server))
    get_parent = parent.uncached_getter()

    @util.cached_property
    def episode(self, copy_server=False):
        _s = server
        if copy_server:
            _s = server.copy()
        eps = _s.eval(
                "@SOBJECT(vfx/sequence.vfx/episode)", self.__search_key__)
        return Episode(eps[0]) if eps else None
    get_episode = episode.uncached_getter()

    @util.cached_property
    def preceding(self):
        objs = self.filter(
                filters=[
                    ('sort_order', '<', self.sort_order),
                    ('sequence_code', self.sequence_code)],
                order_bys=['sort_order desc'])
        if objs:
            objs = [obj for obj in objs
                    if obj.sort_order == objs[0].sort_order]
        return objs
    get_preceding = preceding.uncached_getter()

    @util.cached_property
    def succeeding(self):
        objs = self.filter(
                filters=[
                    ('sort_order', '>', self.sort_order),
                    ('sequence_code', self.sequence_code)],
                order_bys=['sort_order'])
        if objs:
            objs = [obj for obj in objs
                    if obj.sort_order == objs[0].sort_order]
        return objs
    get_succeeding = succeeding.uncached_getter()

    @util.cached_property
    def overlapping(self):
        return [Shot(seq) for seq in self.filter([
            ('sort_order', self.sort_order),
            ('sequence_code', self.sequence_code),
            ('code', '!=', self.code)])]
    get_overlapping = overlapping.uncached_getter


class EpisodeAsset(core.IEpisodeAsset, SObject):
    asset = SObjectField('asset_code')
    episode = SObjectField('episode_code')
    __stype__ = 'vfx/asset_in_episode'

    def __init__(self, *args, **kwargs):
        super(EpisodeAsset, self).__init__(*args, **kwargs)


class SequenceAsset(core.ISequenceAsset, SObject):
    asset = SObjectField('asset_code')
    sequence = SObjectField('sequence_code')
    __stype__ = 'vfx/asset_in_sequence'

    def __init__(self, *args, **kwargs):
        super(SequenceAsset, self).__init__(*args, **kwargs)


class ShotAsset(core.IShotAsset, SObject):
    asset = SObjectField('asset_code')
    sequence = SObjectField('shot_code')
    __stype__ = 'vfx/asset_in_shot'

    def __init__(self, *args, **kwargs):
        super(ShotAsset, self).__init__(*args, **kwargs)


class _Naming(SObject):
    # don't add to the backend instance
    __stype__ = 'vfx/naming'

    def __init__(self, *args, **kwargs):
        super(_Naming, self).__init__(*args, **kwargs)


class User(core.IUser, SObject):
    __stype__ = 'sthpw/login'
    username = SObjectField('login')

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    @classmethod
    def create(cls):  # don't need this
        pass

    @classmethod
    def createMultiple(cls):  # don't need this
        pass


class Group(core.IGroup, SObject):
    __stype__ = 'sthpw/login_group'
    name = SObjectField('code')

    def __init__(self, *args, **kwargs):
        super(Group, self).__init__(*args, **kwargs)

    def addUsers(self, users):
        '''
        adds a user to the group
        args:
            users (list): list of usernames
        '''
        GroupUser.createMultiple([{'login_group': self.name, 'login': user}
                                  for user in users])

    def removeUsers(self, users):
        gu = GroupUser.filter(filters=[
                                ('login_group', self.name),
                                ('login', users)])
        GroupUser.removeMultiple(gu)

    def getUsers(self):
        '''
        returns the list of Users in a group
        '''
        # TODO: User instances can be returned
        return [gu.login for gu in GroupUser.filter(filters=[
                                        ('login_group', self.name)])]


class GroupUser(core.IGroupUser, SObject):
    __stype__ = 'sthpw/login_in_group'
    username = SObjectField('login')
    group = SObjectField('login_group')

    def __init__(self, *args, **kwargs):
        super(GroupUser, self).__init__(*args, **kwargs)


class Pipeline(core.IPipeline, SObject):
    __stype__ = 'sthpw/pipeline'

    def __init__(self, *args, **kwargs):
        super(Pipeline, self).__init__(*args, **kwargs)


tactic_backend.addClass(Project)
tactic_backend.addClass(Snapshot)
tactic_backend.addClass(Asset)
tactic_backend.addClass(Category)
tactic_backend.addClass(Episode)
tactic_backend.addClass(Sequence)
tactic_backend.addClass(Shot)
tactic_backend.addClass(EpisodeAsset)
tactic_backend.addClass(SequenceAsset)
tactic_backend.addClass(ShotAsset)
tactic_backend.addClass(User)
tactic_backend.addClass(Group)
tactic_backend.addClass(Pipeline)
tactic_backend.addClass(File)
