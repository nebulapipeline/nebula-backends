import unittest
import os
import nebula.auth.user as user
from mock import patch

if not user.user_registered():
    user.login('admin', 'aa')
server = user.get_server()
server.set_project('admin')

import nebula.common.core as core
import nebula.backends as backends

core.BackendRegistry.register(backends.tactic_backend)
core.BackendRegistry.set('tactic')
tb = core.BackendRegistry.get()

class TacticTestCase(unittest.TestCase):

    @patch('nebula.backends.src.tactic_backend.server')
    def test_project_creation(self, mocked_server):
        tb.Project.create('Test Pro', 'P:/external',
                            '',
                            template=tb.Project.TEMPLATE_COMMERCIAL,
                            create_shortcuts=False)
        mocked_server.execute_cmd.assert_called_once()
        mocked_server.insert.assert_called_once_with('vfx/episode',
                                {'code': 'default'})

    def test_project_shortcuts_creation(self):
        pro = tb.Project.get(filters=[('code', 'with_shortcut_9')])
        client_repo_dir = server.get_base_dirs()['win32_client_repo_dir']
        self.assertRaises(WindowsError, pro.createShortcuts,
                    'P:/external/Suntop_Season_04')
        shortcut = os.path.join(client_repo_dir, pro.code)
        self.assertTrue(os.path.exists(shortcut))

        # remove the shortcut and create again
        os.rmdir(shortcut)
        os.rmdir(os.path.join(os.path.join(os.path.dirname(
                            os.path.dirname(shortcut)), 'unc'),
                    pro.code))
        self.assertFalse(os.path.exists(shortcut))
        pro.createShortcuts('P:/external/Suntop_Season_04')
        self.assertTrue(os.path.exists(shortcut))

    def test_naming_creation(self):
        pro = tb.Project.get(filters=[('code', 'second')])
        pro.setProject('second')
        pro.createNaming('D:/temp/config_naming_project_second_table.csv')
